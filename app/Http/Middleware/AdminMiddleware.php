<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
 
        if(!Auth::check($id)){
            return redirect('/');
        }
        
        if(!Auth::user()->access){
            return redirect('dashboard');
        }
    $user = Auth::user();
        return $next($request)->with('user', $user);
    }
    }

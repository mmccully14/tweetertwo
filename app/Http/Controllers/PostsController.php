<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use \App\Post;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
	public function showAdmin()
	{
		return "very secert stuff";
	}

	public function index()
	{
	
		 $posts = Post::all();


		$data['posts'] = $posts;

		return view('posts.index', $data);
	}

	public function show($id)
	{
		$post= Post::find($id);

		$data['comments'] = $post->comments;


		$data['post']= $post;

		return view('posts.show', $data);
	}
	public function create()
	{
		return view('posts.create');
	}
	public function edit($id){
		
		$post= Post::find($id);

		$data['post'] = $post;

		return view('posts.edit', $data);
	}

	public function store(Request $request)
	{

	  	$post = new Post;

		$post->title = $request->input('title'); 
		$post->body = $request->input('body');



		if($post->save()) {

			return redirect('/posts');

		} else {

		}
	}
	public function update(Request $request, $id){

		$validator = Validator::make($request->all(), [

			'title' => 'required|max:255',
			'body' => 'required',
		]);

		if($validator->fails()) {
			return redirect('/post/' .$id.'/edit')
			->withErrors($validator)
			->withInput();

		}


		$post = Post::find($id);

		$post->title = $request->title;
		$post->body = $request->body;

		if ($post->save()) {

			return redirect('/post/'.$id.'/edit');

		}	
	}

		public function storeComment(Request $request)
		{
			$comment = new \App\Comment;

			$comment->user_id = Auth::user()->id;
			$comment->post_id = $request->input('post_id') ;
			$comment->body = $request->input('body');

			$comment->save();

			return redirect('/post/' .$request->input('post_id') );

		}
	}







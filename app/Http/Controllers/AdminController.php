<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;

class AdminController extends Controller
{


	public function index()

	{
	
		$data['users'] = User::all();

		return view('users.index', $data);
	}	


	public function edit($id)
	{
		$data['user'] = User::find($id);
		$data['roles'] = \App\Role::all();

			return view('users.edit', $data);
	}
	public function update(Request $request, $id)
	{

		$validator = Validator::make($request->all(), [

			'name' => 'required|max:255',
			'email' => 'required|email',
			'role_id' => 'required|numeric',
			'id' => 'required|numeric',
		]);

		if($validator->fails()) {
			return redirect('/users/'.$id. '/edit')
			->withErrors($validator)
			->withInput();

		}
		
	

	$user = User::find($id);

	$user->name = $request->input('name');
	$user->email = $request->input('email');
	$user->role_id = $request->input('role_id');

	$user->save();

	return redirect('/users');
	
}
public function delete($id)
	{
		$user = User::find($id);

		$user->destroy();

		return redirect('/users');
	}
}

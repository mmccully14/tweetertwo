@extends('layout.master')

@section('layout.sidebar')

@section('title')

@section('content')






<div class="fh5co-about-animate-box">
	<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
		<h2> Edit Post</h2><br>
	</div>

		<div class="container" style="margin-bottom: 18px;">
			<div class="col-md-8 col-md-offset-2 aminate-box">
				<div class="row">

		<form  action="{{ url('/post/'.$post->id) }}" method="post">
		@csrf
		<div class="col-md-12">
			 <div class="form-group row">
                            <label for="title" class="col-sm-4 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ (old('title') ) ? old('title') :$post->title }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="body" class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <textarea id="body" type="body" class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" required>{{ (old('title') ) ? old('body') :$post->body }} </textarea>

                                @if ($errors->has('body'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
			<div class="col-md-12">
				<div class="form-group">
					<input value="Update Post" class="btn btn-primary" type="submit">
                    

			    
         
	</div>
		</form>

		    </div>

		</div>
</div>
@endsection
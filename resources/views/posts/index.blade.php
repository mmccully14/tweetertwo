@extends('layout.master')
@section('content')
<div class="box">
  <h2>All Posts</h2>
  <ul>
    @foreach ($posts as $post)
    <li>
      <a href="#" class="image"><img src="css/images/car4.jpg" alt="" /></a>
      <div class="info">
        
        <p>{{ $post->body }}</p>
      
       @guest
              <li class="up"><a href="{{ url('/post/'.$post->id) }}">View Post</a></li><br>
          @else
               <li class="up"><a href="{{ url('/post/'.$post->id) }}">View Post</a></li><br>
          <li>
              <a href="{{ url('/post/' . $post->id .'/edit') }}" class="btn btn-primary" type="submit">Edit</a>
                  @csrf
                  
            </li>
          @endguest 
        <div class="cl">&nbsp;</div>
 
    @endforeach

  </ul>

  <div class="cl">&nbsp;</div>
</div>


@endsection
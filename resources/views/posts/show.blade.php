@extends('layout.master')

@section('layout.sidebar')

@section('title')

@section('content')


	<div class="fh5co-about animtte-box">
		<div class="col-md-8 col-md-offset-2 text-center fh5co-heading" >
			<h2>{{ $post->title }}</h2>
		</div>

	<div class="container" style="margin-bottom: 10px;">
		<div class="col-md-8 col-md-offset-2 animtte-box">
			<p class="pull-left"> {{ $post->body }}</p>
		</div>
	</div>


		<div class="col-md-8 col-md-offset-2 text-center fh5co-heading" >

			<h2>comments</h2>

		</div>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="css/styles.css" />
        
	<div class="container" style="margin-bottom: 10px;">
		@foreach($comments as $comment)

		<div class="col-md-8 col-md-offset-2 animtte-box">
			<p class="pull-left"> {{ $comment->body }}</p>
		</div>
		@endforeach
	</div>



	@guest
	<p>Please Log In to Comment</p>
	@else

	<div class="container" style="margin-bottom: 10px;">
		<div class="col-md-8 col-md-offset-2 animtte-box">

	<form action="{{ url('/comment') }}" method="post">
		@csrf
		<input type="hidden" name="post_id" value="{{ $post->id }}">
		<textarea name="body"></textarea><br><br>
		<button type="submit"> Submit Comment</button>
	</form>

@endguest
@endsection
@extends('layout.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-default">

<div class="panel-body">
You are logged in! as <strong>{{ strtoupper(Auth::user()->type) }}</strong>
Admin Page: <a href="{{ url('/') }}/admin">{{ url('/') }}/admin</a>

</div>
</div>
@endsection

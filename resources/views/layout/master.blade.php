<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>AutoPortal</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="{{ url('/') }}/css/style.css" type="text/css" media="all" />

<script src="{{ url('/') }}/js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/js/jquery-func.js" type="text/javascript"></script>

</head>
<body>

<!-- Shell -->
<div class="shell">
 
  <div id="header">
  
    <h1 id="logo"><a href="#">autoportal your friend on the road</a></h1>

    @include('layout.nav')
   
  </div>

  <div id="content">
   
  @include('layout.sidebar')

    <div id="main">
   
      @yield('content')

    </div>
   
    <div class="cl">&nbsp;</div>
  </div>

</div>

</body>

</html>

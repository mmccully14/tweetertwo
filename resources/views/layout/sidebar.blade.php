<div id="sidebar">
  <!-- Search -->
  <form action="#" class="search" method="post">
    <div class="cl">&nbsp;</div>
    <input type="text" class="field blink" value="search" title="search" />
    <div class="btnp">
      <input type="submit" value="go" />
    </div>
    <div class="cl">&nbsp;</div>
  </form>
  <!-- End Search -->
  <!-- Sign In Links -->
  <div class="links">
    <div class="cl">&nbsp;</div>
    @guest
    <a href="{{ url('/login') }}" class="left">Sign In</a> 
    <a href="{{ url('/register') }}" class="right">Create account</a>
    @else
    <a href="{{ url('/admin') }}" class="right">Edit Roles</a>
    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
      
    Logout
    <form id="frm-logout" action="{{ url('/logout') }}" method="POST" style="display: none;">
    @csrf
    </form>

    @endguest

    <div class="cl">&nbsp;</div>
  </div>
  <!-- End Sign In Links -->
  <!-- Box Latest News -->
  <div class="box">
    <h2>Latest News</h2>
    <ul>
      <li> <a href="#" class="image"><img src="{{ url('/')}}/css/images/thumb1.jpg" alt="" /></a>
      <div class="info">
        <h5><a href="#">Lorem ipsum dolo</a></h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
      </div>
      <div class="cl">&nbsp;</div>
    </li>
    <li> <a href="#" class="image"><img src="{{ url('/')}}/css/images/thumb2.jpg" alt="" /></a>
    <div class="info">
      <h5><a href="#">Dolor amet urna isque</a></h5>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
    </div>
    <div class="cl">&nbsp;</div>
  </li>
  <li> <a href="#" class="image"><img src="{{ url('/')}}/css/images/thumb3.jpg" alt="" /></a>
  <div class="info">
    <h5><a href="#">Molestie id sceler leo</a></h5>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
  </div>
  <div class="cl">&nbsp;</div>
</li>
<li> <a href="#" class="image"><img src="{{ url('/')}}/css/images/thumb4.jpg" alt="" /></a>
<div class="info">
  <h5><a href="#">Sed elementum molesti</a></h5>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</div>
<div class="cl">&nbsp;</div>
</li>
</ul>
<a href="#" class="up">See more</a>
<div class="cl">&nbsp;</div>
</div>
<!-- End Box Latest News -->
<!-- Box Latest Reviews -->
<div class="box">
<h2>Latest Reviews</h2>
<ul>
<li> <a href="#" class="image"><img src="{{ url('/')}}/css/images/thumb5.jpg" alt="" /></a>
<div class="info">
<h5><a href="#">Lorem ipsum dolo</a></h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</div>
<div class="cl">&nbsp;</div>
</li>
<li> <a href="#" class="image"><img src="{{ url('/')}}/css/images/thumb6.jpg" alt="" /></a>
<div class="info">
<h5><a href="#">Dolor amet urna isque</a></h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</div>
<div class="cl">&nbsp;</div>
</li>
<li> <a href="#" class="image"><img src="{{ url('/')}}/css/images/thumb7.jpg" alt="" /></a>
<div class="info">
<h5><a href="#">Molestie id sceler leo</a></h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</div>
<div class="cl">&nbsp;</div>
</li>
<li> <a href="#" class="image"><img src="{{ url('/')}}/css/images/thumb8.jpg" alt="" /></a>
<div class="info">
<h5><a href="#">Sed elementum molesti</a></h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</div>
<div class="cl">&nbsp;</div>
</li>
</ul>
<a href="#" class="up">See more</a>
<div class="cl">&nbsp;</div>
</div>
<!-- End Box Latest Reviews -->
<!-- Box Latest Posts -->
<div class="box">
<h2>Latest Posts</h2>
<ul>
<li>
<h5><a href="#">Lorem ipsum dolo</a></h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</li>
<li>
<h5><a href="#">Dolor amet urna isque</a></h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</li>
<li>
<h5><a href="#">Molestie id sceler leo</a></h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</li>
<li>
<h5><a href="#">Sed elementum molesti</a></h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</li>
<li>
<h5><a href="#">Sed elementum molesti</a></h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed elementum molestie urna, id scelerisque leo </p>
</li>
</ul>
<a href="#" class="up">See more</a>
<div class="cl">&nbsp;</div>
</div>
</div>
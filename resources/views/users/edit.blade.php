@extends('layout.master')

@section('content')

<div class="fh5co-about-animate-box">
	<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
		<h2> Edit User </h2>
	</div>

		<div class="container" style="margin-bottom: 18px;">
			<div class="col-md-8 col-md-offset-2 aminate-box">
				<div class="row">

		<form method="posts" action="{{ url('/user/'.$user->id. '/edit') }}">
		@csrf

		<div class="col-md-12">
			 <div class="form-group">
			 	<label>Name</label>
			 	<input class="form-control" placeholder="Name" type="text" name="name" value="{{ (old('name') ) ? old('name') : $user->name }}">
			 	@if ($errors->has('name'))
			 		<span class="invalid-feedback" role="alert">
			 			<strong>{{ $errors->first('name') }}</strong>
			 		</span>
			 	@endif
			 </div>
		</div><br>
		<div class="col-md-12">
			 <div class="form-group">
			 	<label>Email</label>
			 	<input class="form-control" placeholder="Email" type="text" name="email" value="{{ (old('email') ) ? old('email') : $user->email }}">
			 	@if ($errors->has('email'))
			 		<span class="invalid-feedback" role="alert">
			 			<strong>{{ $errors->first('email') }}</strong>
			 		</span>
			 	@endif
			 </div>
		</div><br>
		<div class="col-md-12">
			 <div class="form-group">
			 	<label>Role</label>
			 	<select name="role_id" class="form-control">
			 	@foreach($roles as $role)
			 		<option value="{{ $role->id }}">
			 		 
			 				{{ ucfirst($role->name) }}
			 			</option>


			 	@endforeach
			 	<div class="col-md-12">
				<div class="form-group">
					<input value="Update Post" class="btn btn-primary" type="submit">



		    </div>

		</div>

@endsection